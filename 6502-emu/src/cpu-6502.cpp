#include "cpu-6502.h"
#include <cstdlib>
#include <cstdint>
#include <exception>
#include <iostream>
#include <sstream>
#include "cartridge.h"
#include "ines.h"
#include "opcodes.h"

Cpu6502::Cpu6502()
	: status_(0), accumulator_(0), x_(0), y_(0)
{
}

void Cpu6502::reset()
{
	status_ = 0;
	accumulator_ = 0;
	x_ = 0;
	y_ = 0;
	stack_ptr_ = 0x01FF - 1;

	pc_ = memory_.read16Bit(ResetVector);
}

int i = 0;

int Cpu6502::processInstruction()
{
	std::stringstream output;
	//output << i++ << "  $" << std::hex << static_cast<int>(oc) << std::dec << std::endl;
	output << "$" << std::hex << static_cast<int>(pc_) << std::dec << std::endl;
	
	opcode oc = readOpcode();
	
	int cycles;
	switch(oc)
	{
	case 0x0:
		cycles = OpCodes::BRK(*this);
		break;
	case 0x01:
		cycles = OpCodes::ORA_IND_X(*this);
		break;
	case 0x05:
		cycles = OpCodes::ORA_ZER(*this);
		break;
	case 0x0A:
		cycles = OpCodes::ASL_A(*this);
		break;
	case 0x10:
		cycles = OpCodes::BPL(*this);
		break;
	case 0x11:
		cycles = OpCodes::ORA_IND_Y(*this);
		break;
	case 0x18:
		cycles = OpCodes::CLC(*this);
		break;
	case 0x20:
		cycles = OpCodes::JSR(*this);
		break;
	case 0x22:
		cycles = OpCodes::KIL(*this);
		break;
	case 0x29:
		cycles = OpCodes::AND_IMM(*this);
		break;
	case 0x40:
		cycles = OpCodes::RTI(*this);
		break;
	case 0x48:
		cycles = OpCodes::PHA(*this);
		break;
	case 0x4C:
		cycles = OpCodes::JMP_ABS(*this);
		break;
	case 0x60:
		cycles = OpCodes::RTS(*this);
		break;
	case 0x68:
		cycles = OpCodes::PLA(*this);
		break;
	case 0x6C:
		cycles = OpCodes::JMP_IND(*this);
		break;
	case 0x6D:
		cycles = OpCodes::ADC_ABS(*this);
		break;
	case 0x78:
		cycles = OpCodes::SEI(*this);
		break;
	case 0x80:
		cycles = OpCodes::NOP(*this);
		break;
	case 0x84:
		cycles = OpCodes::STY_ZER(*this);
		break;
	case 0x85:
		cycles = OpCodes::STA_ZER(*this);
		break;
	case 0x88:
		cycles = OpCodes::DEY(*this);
		break;
	case 0x8A:
		cycles = OpCodes::TXA(*this);
		break;
	case 0x8D:
		cycles = OpCodes::STA_ABS(*this);
		break;
	case 0x8E:
		cycles = OpCodes::STX_ABS(*this);
		break;
	case 0x91:
		cycles = OpCodes::STA_IND_Y(*this);
		break;
	case 0x98:
		cycles = OpCodes::TYA(*this);
		break;
	case 0x9A:
		cycles = OpCodes::TXS(*this);
		break;
	case 0xA0:
		cycles = OpCodes::LDY_IMM(*this);
		break;
	case 0xA2:
		cycles = OpCodes::LDX_IMM(*this);
		break;
	case 0xA5:
		cycles = OpCodes::LDA_ZER(*this);
		break;
	case 0xA6:
		cycles = OpCodes::LDX_ZER(*this);
		break;
	case 0xA8:
		cycles = OpCodes::TAY(*this);
		break;
	case 0xA9:
		cycles = OpCodes::LDA_IMM(*this);
		break;
	case 0xAA:
		cycles = OpCodes::TAX(*this);
		break;
	case 0xAD:
		cycles = OpCodes::LDA_ABS(*this);
		break;
	case 0xC6:
		cycles = OpCodes::DEC_ZER(*this);
		break;
	case 0xC8:
		cycles = OpCodes::INY(*this);
		break;
	case 0xC9:
		cycles = OpCodes::CMP_IMM(*this);
		break;
	case 0xCA:
		cycles = OpCodes::DEX(*this);
		break;
	case 0xCD:
		cycles = OpCodes::CMP_ABS(*this);
		break;
	case 0xCE:
		cycles = OpCodes::DEC_ABS(*this);
		break;
	case 0xD0:
		cycles = OpCodes::BNE(*this);
		break;
	case 0xD8:
		cycles = OpCodes::CLD(*this);
		break;
	case 0xE6:
		cycles = OpCodes::INC_ZER(*this);
		break;
	case 0xE8:
		cycles = OpCodes::INX(*this);
		break;
	case 0xEE:
		cycles = OpCodes::INC_ABS(*this);
		break;
	case 0xF0:
		cycles = OpCodes::BEQ(*this);
		break;
	case 0xF1:
		cycles = OpCodes::SBC_IND_Y(*this);
		break;
	case 0xF6:
		cycles = OpCodes::INC_ZER_X(*this);
		break;
	case 0xFE:
		cycles = OpCodes::INC_ABS_X(*this);
		break;
	default:
		std::cout << "Unhandled opcode: " << oc << std::endl;
		
		break;
	}

	return cycles;
}

void Cpu6502::load(Cartridge &c)
{
	iNesHeader header = c.header();

	memory_.loadRom(c.prg_rom());
}
	
opcode Cpu6502::readOpcode()
{
	opcode oc = memory_.read8Bit(pc_++);

	return oc;
}

void Cpu6502::clearStatusFlags()
{
	status_ = 0;
}

void Cpu6502::setBreakFlag()
{
	status_ |= 0x10;
}

void Cpu6502::setCarryFlag()
{
	status_ |= 0x01;
}

void Cpu6502::setNegativeFlag()
{
	status_ |= 0x40;
}

void Cpu6502::setZeroFlag()
{
	status_ |= 0x02;
}

void Cpu6502::setInteruptDisableFlag()
{
	status_ |= 0x04;
}

void Cpu6502::clearCarryFlag()
{
	status_ &= ~0x01;
}

void Cpu6502::clearDecimalModeFlag()
{
	status_ &= ~0x08;
}

void Cpu6502::setZeroNegativeFlags(uint8_t value)
{
	if(value & 0x40) // Bit 7
		setNegativeFlag();
	
	if(value == 0)
		setZeroFlag();
}

void Cpu6502::push8BitStack(uint8_t value)
{
	// Apparently 6502 does no overflow/underflow checks on the stack.
	memory_.write(stack_ptr_--, value);
}

void Cpu6502::push16BitStack(uint16_t value)
{
	push8BitStack(static_cast<uint8_t>(value & 0x00FF));
	push8BitStack(static_cast<uint8_t>((value & 0xFF00) >> 8));
}

uint8_t Cpu6502::pop8BitStack()
{
	return memory_.read8Bit(++stack_ptr_);
}

uint16_t Cpu6502::pop16BitStack()
{
	uint8_t high = pop8BitStack();
	uint8_t low = pop8BitStack();

	return low | static_cast<uint16_t>(high) << 8;
}
