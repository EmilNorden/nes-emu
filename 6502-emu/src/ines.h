#ifndef INES_H_
#define INES_H_

#include <cstdint>
#include <iosfwd>

enum class Mirroring
{
	None,
	Horizontal,
	Vertical,
};

class iNesHeader
{
private:
	char nes[4];
	uint8_t prg_banks_;
	uint8_t chr_banks_;
	uint8_t flags6;
	uint8_t flags7;
	uint8_t prgRamSize;
	uint8_t	flags9;
	uint8_t flags10;

	bool trainer_present_;

public:
	Mirroring mirroring_;
	bool fourScreenVRAM_;
	bool sramBatteryPresent_;
	
	bool vsUniSystem_;
	bool playChoice10_;

	void Parse(std::basic_istream<char, std::char_traits<char>> &in);

	uint8_t prg_banks() const { return prg_banks_; }
	uint8_t chr_banks() const { return chr_banks_; }
	bool trainer_present() const { return trainer_present_; }
};

#endif