#include "cpu_memory_controller.h"
#include <cstring>
#include <algorithm>
#define CPU_UBOUND_RAM_MIRRORS		0x1FFF
#define CPU_UBOUND_RAM				0x07FF
#define PPU_UBOUND_IO_MIRRORS		0x3FFF
#define PPU_UBOUND_IO				0x2007

#define PPU_REGISTER_CONTROL1		0x2000
#define PPU_REGISTER_CONTROL2		0x2001
#define PPU_REGISTER_STATUS			0x2002

#define PPU_VBLANK_BIT				0x80

#define PPUADDR						0x2006
#define	PPUDATA						0x2007

#include <sstream>

CpuMemoryController::CpuMemoryController()
{
	memset(ram_.data(), 0, 0xFFFF);
}

uint8_t CpuMemoryController::write(address addr, uint8_t value)
{
	std::stringstream output;
	output  << "write $" << std::hex << static_cast<int>(addr) << std::dec << " (" << (int)value << ")" << std::endl;

	if((addr & CPU_UBOUND_RAM_MIRRORS) == addr) // Write in range [$0000 - $1FFF], mirrored 4 times in 2kb blocks
		ram_[addr & CPU_UBOUND_RAM] = value; // Mask with 0x07FF (2047), leaving the base address in the [$0000 - $07FF] range
	else if((addr & PPU_UBOUND_IO_MIRRORS) == addr) // Write in range [0x2000 - $3FFF] range, mirrored 1024 times in 8b blocks
		ram_[addr & PPU_UBOUND_IO] = value;
	else
	{
		auto callback = write_callbacks_.find(addr);
		if(callback == write_callbacks_.end())
			ram_[addr] = value;
		else
			callback->second(addr, value);
	}
		

	return value;
}

uint8_t CpuMemoryController::read8Bit(address addr)
{
	uint8_t value = ram_[addr];

	if(addr == PPU_REGISTER_STATUS)
		ram_[addr] &= ~PPU_VBLANK_BIT;

	return value;
}

uint16_t CpuMemoryController::read16Bit(address addr)
{
	uint8_t loByte = ram_[addr];
	uint8_t hiByte = ram_[addr + 1];

	return (static_cast<uint16_t>(hiByte) << 8) | loByte;
}

void CpuMemoryController::setWriteCallback(address addr, const std::function<void(address, uint8_t)> &callback)
{
	write_callbacks_[addr] = callback;
}

void CpuMemoryController::loadRom(const char *data)
{
	// Since i know that the game im emulating only has 16k of PRG ROM, in this case I can
	// just copy it to both 0x8000 AND 0xC000.
	// In reality, I shouldnt be copying at all, instead reads from these addresses should
	// "consult" the cartridge mapper to redirect the read to the PRG ROM.
	std::copy(data, data + 0x4000, ram_.data() + 0x8000);
	std::copy(data, data + 0x4000, ram_.data() + 0xC000);
	//memcpy_s(ram_.data() + 0x8000, 0x4000, data, 0x4000);
	//memcpy_s(ram_.data() + 0xC000, 0x4000, data, 0x4000);
}
