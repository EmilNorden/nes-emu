#ifndef CPU_MEMORY_CONTROLLER
#define CPU_MEMORY_CONTROLLER

#include <cstdint>
#include <array>
#include <map>
#include <functional>
typedef uint16_t address;


class CpuMemoryController
{
private:
	std::array<uint8_t, 0x10000> ram_;
	std::map<address, std::function<void(address, uint8_t)>> write_callbacks_;
public:
	CpuMemoryController();

	uint8_t write(address addr, uint8_t value);
	uint8_t read8Bit(address addr);
	uint16_t read16Bit(address addr);
	void setWriteCallback(address addr, const std::function<void(address, uint8_t)> &callback);

	void loadRom(const char *data);
};

#endif