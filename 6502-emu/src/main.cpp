#include "cpu-6502.h"
#include "cartridge.h"
#include <fstream>
#include <SDL.h>
#include "window.h"
#include "glrenderer.h"

#include "nes.h"

#include <sstream>

int main(int argc, char **argv)
{
	SDL_Init(SDL_INIT_VIDEO);

	Window win("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	GLRenderer renderer;

	renderer.init(win);

	
	std::ifstream file;

	file.open("c:/temp/dk.nes", std::ios::binary);

	Cartridge cart(file);
	
	Nes nes;

	nes.load(cart);

	GLuint texture_;
	glGenTextures(1, &texture_);
	bool run = true;
	while(run)
	{
		renderer.clear();

		SDL_Event e;
		while(SDL_PollEvent(&e))
		{
			if(e.type == SDL_QUIT)
				run = false;			
		}
		
		
		nes.runOnce();
		const uint32_t *p = nes.back_buffer();

		glBindTexture(GL_TEXTURE_2D, texture_);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 256, 240, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, p);
		auto i = glGetError();
		renderer.renderTexture(texture_, 0, 0, 800, 600);
		auto j = glGetError();
		renderer.present(win);

		
		
	}

	

	file.close();

	SDL_Quit();

	return 0;
}
