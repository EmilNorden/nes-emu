#ifndef OPCODES_H_
#define OPCODES_H_

#include <cstdint>

class Cpu6502;

//struct OpCodeData
//{
//	int8_t pc_offset;
//	uint8_t cycles;
//};
 

class OpCodes
{
private:
	static uint16_t absolute(Cpu6502 &cpu);
	static uint16_t indexedIndirect(Cpu6502 &cpu);
	static uint16_t indirectIndexed(Cpu6502 &cpu);

	static void ORA(Cpu6502 &cpu, uint8_t value);
	static void AND(Cpu6502 &cpu, uint8_t value);
	static void JMP(Cpu6502 &cpu, uint16_t addr);
	static void ADC(Cpu6502 &cpu, uint8_t value);
	static void CMP(Cpu6502 &cpu, uint8_t value);
	static void EOR(Cpu6502 &cpu, uint8_t value);
	static void DEC(Cpu6502 &cpu, uint16_t addr);
	static void SBC(Cpu6502 &cpu, uint8_t value);

	static int branchToOffset(Cpu6502 &cpu, bool condition);
public:
	static int ADC_ABS(Cpu6502 &cpu);
	static int ASL_A(Cpu6502 &cpu);
	static int AND_IMM(Cpu6502 &cpu);
	static int BEQ(Cpu6502 &cpu);
	static int BNE(Cpu6502 &cpu);
	static int BPL(Cpu6502 &cpu);
	static int BRK(Cpu6502 &cpu);
	static int CLC(Cpu6502 &cpu);
	static int CLD(Cpu6502 &cpu);
	static int CMP_ABS(Cpu6502 &cpu);
	static int CMP_IMM(Cpu6502 &cpu);
	static int DEC_ABS(Cpu6502 &cpu);
	static int DEC_ZER(Cpu6502 &cpu);
	static int DEX(Cpu6502 &cpu);
	static int DEY(Cpu6502 &cpu);
	static int EOR_ABS(Cpu6502 &cpu);
	static int EOR_ABS_X(Cpu6502 &cpu);
	static int EOR_ABS_Y(Cpu6502 &cpu);
	static int EOR_IMM(Cpu6502 &cpu);
	static int EOR_IND_X(Cpu6502 &cpu);
	static int EOR_IND_Y(Cpu6502 &cpu);
	static int EOR_ZER(Cpu6502 &cpu);
	static int EOR_ZER_X(Cpu6502 &cpu);
	static int INC_ABS(Cpu6502 &cpu);
	static int INC_ABS_X(Cpu6502 &cpu);
	static int INC_ZER(Cpu6502 &cpu);
	static int INC_ZER_X(Cpu6502 &cpu);
	static int INX(Cpu6502 &cpu);
	static int INY(Cpu6502 &cpu);
	static int JMP_ABS(Cpu6502 &cpu);
	static int JMP_IND(Cpu6502 &cpu);
	static int JSR(Cpu6502 &cpu);
	static int KIL(Cpu6502 &cpu);
	static int LDA_ABS(Cpu6502 &cpu);
	static int LDA_IMM(Cpu6502 &cpu);
	static int LDA_ZER(Cpu6502 &cpu);
	static int LDX_IMM(Cpu6502 &cpu);
	static int LDX_ZER(Cpu6502 &cpu);
	static int LDY_IMM(Cpu6502 &cpu);
	static int NOP(Cpu6502 &cpu);
	static int ORA_IND_X(Cpu6502 &cpu);
	static int ORA_IND_Y(Cpu6502 &cpu);
	static int ORA_ZER(Cpu6502 &cpu);
	static int PHA(Cpu6502 &cpu);
	static int PLA(Cpu6502 &cpu);
	static int RTI(Cpu6502 &cpu);
	static int RTS(Cpu6502 &cpu);
	static int SBC_IND_Y(Cpu6502 &cpu);
	static int SEI(Cpu6502 &cpu);
	static int STA_ABS(Cpu6502 &cpu);
	static int STA_IND_Y(Cpu6502 &cpu);
	static int STA_ZER(Cpu6502 &cpu);
	static int STX_ABS(Cpu6502 &cpu);
	static int STY_ZER(Cpu6502 &cpu);
	static int TAX(Cpu6502 &cpu);
	static int TAY(Cpu6502 &cpu);
	static int TXA(Cpu6502 &cpu);
	static int TXS(Cpu6502 &cpu);
	static int TYA(Cpu6502 &cpu);
};

#endif