#ifndef GL_RENDERER_H_
#define GL_RENDERER_H_

#include "sdl/SDL.h"
#include "sdl/SDL_opengl.h"
#include <memory>

class GUIText;
class Window;

class GLRenderer
{
private:
	SDL_GLContext context_;
public:
	GLRenderer();
	~GLRenderer();

	void init(const Window &window);

	void renderTexture(GLuint texture, int x, int y, int w, int h) const;
	void clear() const;
	void present(const Window &window) const;
};

#endif