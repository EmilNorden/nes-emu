#include "ppu-2C02.h"
#include "cartridge.h"
#include <algorithm>
#include <cstring>

PpuMemoryController::PpuMemoryController()
	:
	first_write_(true),
	ppuaddr_increment_(1),
	base_nametable_addr_(0x2000),
	v_(0),
	t_(0),
	x_(0)
{
}

void PpuMemoryController::loadRom(const char *data)
{
	// Originally I copied the data to 0x2000 in PPU address space
	// I think thats wrong. The CHR bank starts at 0x0.
	// HOWEVER, as with the cpu memory, I shouldnt copy the data to the ppu memory,
	// I should consult the cartridges mapper.
	std::copy(data, data + 0x2000, ram_.data());
	//std::copy(data, data + 0x2000, ram_.data() + 0x2000);
}

uint8_t PpuMemoryController::read8Bit(address addr)
{
	return ram_[addr & 0x3FFF];
}

uint16_t PpuMemoryController::read16Bit(address addr)
{
	uint8_t loByte = read8Bit(addr);
	uint8_t hiByte = read8Bit(addr + 1);

	return (static_cast<uint16_t>(hiByte) << 8) | loByte;
}

uint8_t PpuMemoryController::write(address addr, uint8_t value)
{
	return ram_[addr & 0x3FFF] = value;
}

void PpuMemoryController::copyHorizontalScrollingBits()
{
	// copy all bits related to horizontal scrolling
	uint16_t mask = 0x041F;
	v_ = (v_ & ~mask) | (t_ & mask);
}

void PpuMemoryController::copyVerticalScrollingBits()
{
	// copy all bits related to vertical scrolling
	uint16_t mask = 0x7BE0;
	v_ = (v_ & ~mask) | (t_ & mask);
}

void PpuMemoryController::incrementX()
{
	// from the skinny on nes scrolling
	if((v_ & 0x001F) == 31) // if coarse X == 31
	{
		v_ &= ~0x001F; // coarse X = 0
		v_ ^= 0x0400; // switch horizontal nametable
	}
	else
	{
		v_ += 1;
	}
}

void PpuMemoryController::incrementY()
{
	// from the skinny on nes scrolling
	if((v_ & 0x7000) != 0x7000) // if fine Y < 7
	{
		v_ += 0x1000; // increment fine Y
	}
	else
	{
		v_ &= ~0x7000; // fine Y = 0
		int y = (v_ & 0x03E0) >> 5; // y = coarse Y
		if(y == 29)
		{
			y = 0; // coarse Y = 0
			v_ ^= 0x8000; // switch vertical nametable
		}
		else if(y == 31)
		{
			y = 0; // coarse Y = 0, nametable not switched
		}
		else
		{
			y += 1;
		}
		v_ = (v_ & ~0x03E0) | (y << 5);
	}
}

void PpuMemoryController::clearVBlank()
{
	ppu_registers_[PPUSTATUS] &= ~0x80;
}

void PpuMemoryController::setVBlank()
{
	ppu_registers_[PPUSTATUS] |= 0x80;
}

void PpuMemoryController::ppuctrlWrite(address addr, uint8_t value)
{
	ppu_registers_[PPUCTRL] = value;

	// Mask out all but bit 10-11 of t_. Mask out lower 2 bits of value, shift up to position 10-11, and OR them.
	t_ = (t_ & 0x73FF) | ((value & 0x03) << 10);
}

void PpuMemoryController::ppumaskWrite(address addr, uint8_t value)
{
	ppu_registers_[PPUMASK] = value;
}

void PpuMemoryController::oamaddrWrite(address addr, uint8_t value)
{
	ppu_registers_[OAMADDR] = value;
}

void PpuMemoryController::oamdataWrite(address addr, uint8_t value)
{

}

void PpuMemoryController::ppuscrollWrite(address addr, uint8_t value)
{
	if(first_write_)
	{
		ppu_registers_[PPUSCROLL] |= value << 8;

		// value will be split in two parts, the top 5 bits goes into t_, the bottom 3 goes into x_.

		// Mask out all but the first 5 bits of t_. Mask out the last 5 bits of value, shift down to position 0, and OR them.
		t_ = (t_ & 0xFFE0) | ((value & 0xF8) >> 3);
		// Store bottom 3-bit value in x_.
		x_ = value & 0x07;
	}
	else
	{
		ppu_registers_[PPUSCROLL] |= value;

		uint16_t fine_y = (value & 0x07) << 12;
		uint16_t coarse_y = (value & 0xF8) << 2;
		t_ = (t_ & 0x0C1F) | coarse_y | fine_y;	
	}

	first_write_ = !first_write_;
}

void PpuMemoryController::ppuaddrWrite(address addr, uint8_t value)
{
	if(first_write_)
	{
		current_vram_addr_ |= value << 8;

		// Mask out lower 8 bits of t_. OR 'value' into upper 8 bits. Leave the very top bit 0.
		t_ = (t_ & 0xFF) | ((value & 0x3F) << 8);
	}
	else
	{
		current_vram_addr_ |= value;

		t_ = (t_ & 0xFF00) | value;
		v_ = t_;
	}

	first_write_ = !first_write_;
}

void PpuMemoryController::ppudataWrite(address addr, uint8_t value)
{
	write(current_vram_addr_, value);
	current_vram_addr_ += ppuaddr_increment_;
}

Ppu2C02::Ppu2C02()
{
	reset();
}

void Ppu2C02::load(Cartridge &c)
{
	memory_.loadRom(c.chr_rom());
}

void Ppu2C02::reset()
{
	scanline_ = -1;
	pixel_ = 0;
	scanline_cycle_ = 0;
	cycle_remainder_ = 0;
	fetch_state_ = FetchState::NameTable;
	render_ = true;
	frames = 0;
}

int Ppu2C02::fetch_background(int cycles)
{
	if(cycles > 1 && fetch_state_ == FetchState::NameTable)
	{
		uint16_t addr = 0x2000 | (memory_.v_ & 0x0FFF);
		nt_latch_ = memory_.read8Bit(addr);
		scanline_cycle_ += 2;
		cycles -= 2;

		fetch_state_ = FetchState::AttributeTable;
	}

	if(cycles > 1 && fetch_state_ == FetchState::AttributeTable)
	{
		// 0x0C00 == 0b110000000000
		// 0x38   == 0b111000

		// 0x23C0 | name table select |		
		uint16_t addr = 0x23C0 | (memory_.v_ & 0x0C00) | ((memory_.v_ >> 4) & 0x38) | ((memory_.v_ >> 2) & 0x07);
		at_latch_ = memory_.read8Bit(addr);
		scanline_cycle_ += 2;
		cycles -= 2;

		fetch_state_ = FetchState::TileLow;
	}

	if(cycles > 1 && fetch_state_ == FetchState::TileLow)
	{
		uint16_t addr = 0x2000 | (memory_.v_ & 0x0FFF);
		bg_lo_latch_ = memory_.read8Bit(addr);
		scanline_cycle_ += 2;
		cycles -= 2;

		fetch_state_ = FetchState::TileHigh;
	}

	if(cycles > 1 && fetch_state_ == FetchState::TileHigh)
	{
		uint16_t addr = 0x2000 | (memory_.v_ & 0x0FFF);
		bg_hi_latch_ = memory_.read8Bit(addr);
		scanline_cycle_ += 2;
		cycles -= 2;

		fetch_state_ = FetchState::FetchEnd;
	}

	if(fetch_state_ == FetchState::FetchEnd)
	{
		background_shift8[0] = nt_latch_;
		background_shift8[1] = at_latch_;
		background_shift16[0] = (background_shift16[0] & 0x00FF) | (bg_lo_latch_ << 8);
		background_shift16[1] = (background_shift16[1] & 0x00FF) | (bg_hi_latch_ << 8);

		fetch_state_ = FetchState::NameTable;

		// fine x selects a bit, so I right shift X times and AND out the first bit.
		uint8_t bg_lo = (background_shift16[0] >> memory_.x_) & 0x01;
		uint8_t bg_hi = (background_shift16[1] >> memory_.x_) & 0x01;

		uint8_t nt = (background_shift8[0] >> memory_.x_) & 0x01;
		uint8_t at = (background_shift8[1] >> memory_.x_) & 0x01;

		

		background_shift16[0] >>= 1;
		background_shift16[1] >>= 1;
		background_shift8[0] >>= 1;
		background_shift8[1] >>= 1;
	}

	return cycles;
}

void Ppu2C02::process(int cycles)
{
	cycles += cycle_remainder_;

	if(is_dummy_scanline() || is_visible_scanline())
	{
		if(scanline_cycle_ == 0) // Idle cycle
		{
			scanline_cycle_++;
			cycles--;
		}

		if(scanline_cycle_ > 0 && scanline_cycle_ <= 256) // Cycle 0-256, background fetching
		{
			cycles = fetch_background(cycles);
		}

		if(scanline_cycle_ > 256 && scanline_cycle_ <= 320) // Cycle 257 - 320, Sprite fetching for next scanline
		{
			if(cycles > 1 && fetch_state_ == FetchState::NameTable)
			{
				scanline_cycle_ += 2;
				cycles -= 2;

				fetch_state_ = FetchState::AttributeTable;
			}

			if(cycles > 1 && fetch_state_ == FetchState::AttributeTable)
			{
				scanline_cycle_ += 2;
				cycles -= 2;

				fetch_state_ = FetchState::TileLow;
			}

			if(cycles > 1 && fetch_state_ == FetchState::TileLow)
			{
				uint16_t addr = 0x2000 | (memory_.v_ & 0x0FFF);
				bg_lo_latch_ = memory_.read8Bit(addr);
				scanline_cycle_ += 2;
				cycles -= 2;

				fetch_state_ = FetchState::TileHigh;
			}

			if(cycles > 1 && fetch_state_ == FetchState::TileHigh)
			{
				uint16_t addr = 0x2000 | (memory_.v_ & 0x0FFF);
				bg_hi_latch_ = memory_.read8Bit(addr);
				scanline_cycle_ += 2;
				cycles -= 2;

				fetch_state_ = FetchState::NameTable;
			}
		}

		if(scanline_cycle_ > 320 && scanline_cycle_ <= 336) // Cycle 321 - 336, background fetching for first two tiles of next scanline
		{
			cycles = fetch_background(cycles);
		}

		if(scanline_cycle_ > 336 && scanline_cycle_ <= 340) // Cycle 337 - 340, garbage (?) byte reads.
		{
			// Two nametable bytes are fetched (the same nametable bytes that will be fetched first on the next scanline)
			// The purpose seems to be unknown, so for now I will not fetch any bytes, just "spend" cycles as if I had.

			if(cycles > 4)
			{
				cycles -= 4;
				scanline_cycle_ += 4;
			}
		}
	}
	
	if(is_post_render_scanline())
	{
		int remaining_cycles = 341 - scanline_cycle_;
		int cycles_to_spend = std::min(remaining_cycles, cycles);

		cycles -= cycles_to_spend;
		scanline_cycle_ += cycles_to_spend;
	}

	if(is_vblank_scanline())
	{
		// Step through the first two cycles, so we can set VBlank on the second.
		if(scanline_cycle_ == 0 && cycles > 0)
		{
			scanline_cycle_++;
			cycles--;
		}

		if(scanline_cycle_ == 1 && cycles > 0)
		{
			scanline_cycle_++;
			cycles--;

			memory_.setVBlank();
			// VBlank NMI occurs here.
		}

		if(scanline_cycle_ > 1)
		{
			int remaining_cycles = 341 - scanline_cycle_;
			int cycles_to_spend = std::min(remaining_cycles, cycles);

			cycles -= cycles_to_spend;
			scanline_cycle_ += cycles_to_spend;
		}
	}

	if(scanline_cycle_ == 341)
	{
		scanline_cycle_ = 0;
		scanline_ = (scanline_ + 1) % 261;
		for(int x = 0; x < 256; ++x)
		{
			videobuffer_[(scanline_ * 256) + x] = 0xFF0000FF;
		}
	}

	if(render_)
	{
		if(scanline_ > -1 && scanline_ < 240)
		{
			
		}


		if(scanline_cycle_ == 256)
		{
			memory_.incrementY();
		}
		else if(scanline_cycle_ == 257)
		{
			memory_.copyHorizontalScrollingBits();
		}

		if(scanline_ == 261) // Pre-render scanline? or is it -1?
		{
			if(scanline_cycle_ >= 280 && scanline_cycle_ <= 304)
			{
				memory_.copyVerticalScrollingBits();
			}
		}

		if((scanline_cycle_ >= 328 || scanline_cycle_ <= 256) && (scanline_cycle_ % 8) == 0)
		{
			memory_.incrementX();
		}
	}

	cycle_remainder_ = cycles;
}

void Ppu2C02::process2(int cycles)
{
}
