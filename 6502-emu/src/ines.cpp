#include "ines.h"
#include <fstream>
#include <exception>

void iNesHeader::Parse(std::basic_istream<char, std::char_traits<char>> &in)
{
	in.read(nes, 4);
	in.read((char*)&prg_banks_, 1);
	in.read((char*)&chr_banks_, 1);
	in.read((char*)&flags6, 1);
	in.read((char*)&flags7, 1);
	in.read((char*)&prgRamSize, 1);
	in.read((char*)&flags9, 1);
	in.read((char*)&flags10, 1);
	in.ignore(5); // 5 NULL bytes

	if((flags6 ^ 0x09))
		mirroring_ = Mirroring::Horizontal;
	else if(flags6 & 0x01)
		mirroring_ = Mirroring::Vertical;
	else
		mirroring_ = Mirroring::None;

	 fourScreenVRAM_ = flags6 & 0x08;
	 sramBatteryPresent_ = flags6 & 0x02;
	 trainer_present_ = flags6 & 0x04;
	 uint8_t mapper = (flags6 & 0xF0) >> 4;
}