#include "opcodes.h"
#include "cpu-6502.h"

#include <stdexcept>

address OpCodes::absolute(Cpu6502 &cpu)
{
	return cpu.memory_.read16Bit(cpu.pc_);
}

address OpCodes::indexedIndirect(Cpu6502 &cpu) //  ($c0,X)
{
	return cpu.memory_.read16Bit(cpu.memory_.read8Bit(cpu.pc_) + cpu.x_);
}

address OpCodes::indirectIndexed(Cpu6502 &cpu) // ($c0),Y
{
	return cpu.memory_.read16Bit(cpu.memory_.read8Bit(cpu.pc_)) + cpu.y_;
}

void OpCodes::ORA(Cpu6502 &cpu, uint8_t value)
{
	cpu.accumulator_ |= value;
	cpu.setZeroNegativeFlags(cpu.accumulator_);
}

void OpCodes::AND(Cpu6502 &cpu, uint8_t value)
{
	cpu.accumulator_ &= value;
	cpu.setZeroNegativeFlags(cpu.accumulator_);
}

void OpCodes::ADC(Cpu6502 &cpu, uint8_t value)
{
	uint8_t result = cpu.accumulator_ + value;

	if(cpu.carryFlagSet()) // Add carry flag
		result += 1;

	if(result > cpu.accumulator_ && value > 0) // If we didnt wrap around, clear the carry flag.
		cpu.clearCarryFlag();

	cpu.accumulator_ = result;
	cpu.setZeroNegativeFlags(cpu.accumulator_);
}

void OpCodes::CMP(Cpu6502 &cpu, uint8_t value)
{
	if(cpu.accumulator_ == value)
	{
		cpu.setCarryFlag();
		cpu.setZeroFlag();
	}
	else if(cpu.accumulator_ > value)
		cpu.setCarryFlag();

	// Negative flag "Set if bit 7 of the result is set" - What result? The accumulator?
	if(cpu.accumulator_ & 0x40)
		cpu.setNegativeFlag();
}

void OpCodes::EOR(Cpu6502 &cpu, uint8_t value)
{
	cpu.accumulator_ ^= value;
	cpu.setZeroNegativeFlags(cpu.accumulator_);
}

void OpCodes::DEC(Cpu6502 &cpu, address addr)
{
	uint8_t value = cpu.memory_.write(addr, cpu.memory_.read8Bit(addr) - 1);
	cpu.setZeroNegativeFlags(value);
}

void OpCodes::SBC(Cpu6502 &cpu, uint8_t value)
{
	uint8_t result = cpu.accumulator_ - value;

	if(cpu.accumulator_ < value) // If we didnt wrap around, clear the carry flag.
		cpu.clearCarryFlag();
	else
		cpu.setCarryFlag();

	cpu.accumulator_ = result;
	cpu.setZeroNegativeFlags(cpu.accumulator_);
}

void OpCodes::JMP(Cpu6502 &cpu, uint16_t addr)
{
	cpu.pc_ = addr;
}

int OpCodes::branchToOffset(Cpu6502 &cpu, bool condition)
{
	if(condition)
	{
		int8_t offset = cpu.memory_.read8Bit(cpu.pc_++);
		cpu.pc_ += offset;

		return 3;
		//TODO:   ** add 1 to cycles if branch occurs on same page
		// add 2 to cycles if branch occurs to different page.
		// (2 cycler f�r instruktionen, 1 extra vid branching, 2 extra vid branching till annan page).
	}
	else
	{
		cpu.pc_++;

		return 2;
	}
}

/* End of "helper" functions */

int OpCodes::ADC_ABS(Cpu6502 &cpu)
{
	ADC(cpu, cpu.memory_.read8Bit(absolute(cpu)));
	cpu.pc_ += 2;

	return 4;
}

int OpCodes::AND_IMM(Cpu6502 &cpu)
{
	AND(cpu, cpu.memory_.read8Bit(cpu.pc_));

	cpu.pc_++;

	return 2;
}

int OpCodes::ASL_A(Cpu6502 &cpu)
{
	// Shift bit 7 into carry.
	cpu.status_ |= cpu.accumulator_ >> 7;

	cpu.accumulator_ <<= 1;

	return 2;
}

int OpCodes::BEQ(Cpu6502 &cpu)
{
	return branchToOffset(cpu, cpu.zeroFlagSet());
	// TODO:   ** add 1 to cycles if branch occurs on same page
    // add 2 to cycles if branch occurs to different page
}

int OpCodes::BNE(Cpu6502 &cpu)
{
	return branchToOffset(cpu, !cpu.zeroFlagSet());
	// TODO:   ** add 1 to cycles if branch occurs on same page
    // add 2 to cycles if branch occurs to different page
}

int OpCodes::BPL(Cpu6502 &cpu)
{
	return branchToOffset(cpu, !cpu.negativeFlagSet());
	// TODO:   ** add 1 to cycles if branch occurs on same page
    // add 2 to cycles if branch occurs to different page
}

int OpCodes::BRK(Cpu6502& cpu)
{
	cpu.push16BitStack(cpu.pc_);
	cpu.push8BitStack(cpu.status_);
	cpu.pc_ = cpu.memory_.read16Bit(Cpu6502::InterruptVector);
	cpu.setBreakFlag();

	return 7;
}

int OpCodes::CLC(Cpu6502 &cpu)
{
	cpu.clearCarryFlag();

	return 2;
}

int OpCodes::CLD(Cpu6502 &cpu)
{
	cpu.clearDecimalModeFlag();

	return 2;
}

int OpCodes::CMP_ABS(Cpu6502 &cpu)
{
	CMP(cpu, cpu.memory_.read8Bit(absolute(cpu)));

	cpu.pc_ += 2;

	return 4;
}

int OpCodes::CMP_IMM(Cpu6502 &cpu)
{
	CMP(cpu, cpu.memory_.read8Bit(cpu.pc_));
	cpu.pc_++;

	return 2;
}

int OpCodes::DEC_ABS(Cpu6502 &cpu)
{
	DEC(cpu, absolute(cpu));

	cpu.pc_ += 2;

	return 3;
}

int OpCodes::DEC_ZER(Cpu6502 &cpu)
{
	DEC(cpu, cpu.memory_.read8Bit(cpu.pc_));
	
	cpu.pc_++;

	return 5;
}

int OpCodes::DEX(Cpu6502 &cpu)
{
	cpu.x_--;
	cpu.setZeroNegativeFlags(cpu.x_);

	return 2;
}

int OpCodes::DEY(Cpu6502 &cpu)
{
	cpu.y_--;
	cpu.setZeroNegativeFlags(cpu.y_);

	return 2;
}

int OpCodes::EOR_ABS(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(absolute(cpu)));

	cpu.pc_ += 2;

	return 4;
}

int OpCodes::EOR_ABS_X(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(absolute(cpu) + cpu.x_));

	cpu.pc_ += 2;

	return 4; // TODO: Add 1 if it crosses page boundary
}

int OpCodes::EOR_ABS_Y(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(absolute(cpu) + cpu.y_));

	cpu.pc_ += 2;

	return 4; // TODO: Add 1 if it crosses page boundary
}

int OpCodes::EOR_IMM(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(cpu.pc_));

	cpu.pc_++;

	return 2;
}

int OpCodes::EOR_IND_X(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(indexedIndirect(cpu)));

	cpu.pc_++;

	return 5; // TODO: Add 1 if it crosses page boundary
}

int OpCodes::EOR_IND_Y(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(indirectIndexed(cpu)));

	cpu.pc_++;

	return 5; // TODO: Add 1 if it crosses page boundary
}

int OpCodes::EOR_ZER(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(cpu.memory_.read8Bit(cpu.pc_)));

	cpu.pc_++;

	return 3;
}

int OpCodes::EOR_ZER_X(Cpu6502 &cpu)
{
	EOR(cpu, cpu.memory_.read8Bit(cpu.memory_.read8Bit(cpu.pc_) + cpu.x_));

	cpu.pc_++;

	return 4;
}

int OpCodes::INC_ABS(Cpu6502 &cpu)
{
	address addr = absolute(cpu);
	uint8_t value = cpu.memory_.write(addr, cpu.memory_.read8Bit(addr) + 1);
	cpu.setZeroNegativeFlags(value);

	cpu.pc_ += 2;

	return 6;
}

int OpCodes::INC_ABS_X(Cpu6502 &cpu)
{
	address addr = absolute(cpu) + cpu.x_;
	uint8_t value = cpu.memory_.write(addr, cpu.memory_.read8Bit(addr) + 1);
	cpu.setZeroNegativeFlags(value);

	cpu.pc_ += 2;

	return 7;
}

int OpCodes::INC_ZER(Cpu6502 &cpu)
{
	address addr = cpu.memory_.read8Bit(cpu.pc_);
	uint8_t value = cpu.memory_.write(addr, cpu.memory_.read8Bit(addr) + 1);
	cpu.setZeroNegativeFlags(value);

	cpu.pc_++;

	return 5;
}

int OpCodes::INC_ZER_X(Cpu6502 &cpu)
{
	address addr = cpu.memory_.read8Bit(cpu.pc_) + cpu.x_;
	uint8_t value = cpu.memory_.write(addr, cpu.memory_.read8Bit(addr) + 1);
	cpu.setZeroNegativeFlags(value);

	cpu.pc_++;

	return 6;
}

int OpCodes::INX(Cpu6502 &cpu)
{
	cpu.x_++;
	cpu.setZeroNegativeFlags(cpu.x_);

	return 2;
}

int OpCodes::INY(Cpu6502 &cpu)
{
	cpu.y_++;
	cpu.setZeroNegativeFlags(cpu.y_);

	return 2;
}

int OpCodes::JMP_ABS(Cpu6502 &cpu)
{
	JMP(cpu, absolute(cpu));

	return 3;
}

int OpCodes::JMP_IND(Cpu6502 &cpu)
{
	JMP(cpu, cpu.memory_.read16Bit(cpu.memory_.read16Bit(cpu.pc_)));

	return 5;
}

int OpCodes::JSR(Cpu6502 &cpu)
{
	cpu.push16BitStack(cpu.pc_ - 1);
	cpu.pc_ = cpu.memory_.read16Bit(cpu.pc_);

	return 6;
}

int OpCodes::KIL(Cpu6502 &cpu)
{
	throw std::runtime_error("KIL instruction");
}

int OpCodes::LDA_ABS(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.memory_.read8Bit(absolute(cpu));
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	cpu.pc_ += 2;

	return 4;
}

int OpCodes::LDA_IMM(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.memory_.read8Bit(cpu.pc_);
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	cpu.pc_++;

	return 2;
}

int OpCodes::LDA_ZER(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.memory_.read8Bit(cpu.memory_.read8Bit(cpu.pc_));
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	cpu.pc_++;

	return 3;
}

int OpCodes::LDX_IMM(Cpu6502 &cpu)
{
	cpu.x_ = cpu.memory_.read8Bit(cpu.pc_);
	cpu.setZeroNegativeFlags(cpu.x_);

	cpu.pc_++;

	return 2;
}

int OpCodes::LDX_ZER(Cpu6502 &cpu)
{
	cpu.x_ = cpu.memory_.read8Bit(cpu.memory_.read8Bit(cpu.pc_));
	cpu.setZeroNegativeFlags(cpu.x_);

	cpu.pc_++;

	return 3;
}

int OpCodes::LDY_IMM(Cpu6502 &cpu)
{
	cpu.y_ = cpu.memory_.read8Bit(cpu.pc_);
	cpu.setZeroNegativeFlags(cpu.y_);

	cpu.pc_++;

	return 2;
}

int OpCodes::NOP(Cpu6502 &cpu)
{
	// Nothing to see here!
	return 2;
}

int OpCodes::ORA_IND_X(Cpu6502 &cpu)
{
	ORA(cpu, cpu.memory_.read8Bit(indexedIndirect(cpu)));

	cpu.pc_++;
	return 6;
}

int OpCodes::ORA_IND_Y(Cpu6502 &cpu)
{
	ORA(cpu, cpu.memory_.read8Bit(indirectIndexed(cpu)));

	cpu.pc_++;

	return 5;
	//TODO: add 1 to cycles if page boundery is crossed
}

int OpCodes::ORA_ZER(Cpu6502 &cpu)
{
	
	ORA(cpu, cpu.memory_.read8Bit(cpu.memory_.read16Bit(cpu.pc_)));

	cpu.pc_++;
	return 3;
}

int OpCodes::PHA(Cpu6502 &cpu)
{
	cpu.push8BitStack(cpu.accumulator_);

	return 3;
}

int OpCodes::PLA(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.pop8BitStack();
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	return 4;
}

int OpCodes::RTI(Cpu6502 &cpu)
{
	cpu.status_ = cpu.pop8BitStack();
	cpu.pc_ = cpu.pop16BitStack();

	return 6;
}

int OpCodes::RTS(Cpu6502 &cpu)
{
	cpu.pc_ = cpu.pop16BitStack() + 1;

	return 6;
}

int OpCodes::SBC_IND_Y(Cpu6502 &cpu)
{
	SBC(cpu, indirectIndexed(cpu));

	cpu.pc_++;

	return 5; // TODO: Add 1 if crosses page boundary
}

int OpCodes::SEI(Cpu6502 &cpu)
{
	cpu.setInteruptDisableFlag();

	return 2;
}

int OpCodes::STA_ABS(Cpu6502 &cpu)
{
	cpu.memory_.write(absolute(cpu), cpu.accumulator_);

	cpu.pc_ += 2;

	return 4;
}

int OpCodes::STA_IND_Y(Cpu6502 &cpu)
{
	cpu.memory_.write(indirectIndexed(cpu), cpu.accumulator_);

	cpu.pc_++;

	return 6;
}

int OpCodes::STA_ZER(Cpu6502 &cpu)
{
	cpu.memory_.write(cpu.memory_.read8Bit(cpu.pc_), cpu.accumulator_);

	cpu.pc_++;

	return 3;
}

int OpCodes::STX_ABS(Cpu6502 &cpu)
{
	cpu.memory_.write(absolute(cpu), cpu.x_);

	cpu.pc_ += 2;

	return 4;
}

int OpCodes::STY_ZER(Cpu6502 &cpu)
{
	cpu.memory_.write(cpu.memory_.read8Bit(cpu.pc_), cpu.y_);

	cpu.pc_++;

	return 3;
}

int OpCodes::TAX(Cpu6502 &cpu)
{
	cpu.x_ = cpu.accumulator_;
	cpu.setZeroNegativeFlags(cpu.x_);

	return 2;
}

int OpCodes::TAY(Cpu6502 &cpu)
{
	cpu.y_ = cpu.accumulator_;
	cpu.setZeroNegativeFlags(cpu.y_);

	return 2;
}

int OpCodes::TXA(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.x_;
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	return 2;
}

int OpCodes::TXS(Cpu6502 &cpu)
{
	cpu.push8BitStack(cpu.x_);

	return 2;
}

int OpCodes::TYA(Cpu6502 &cpu)
{
	cpu.accumulator_ = cpu.y_;
	cpu.setZeroNegativeFlags(cpu.accumulator_);

	return 2;
}
