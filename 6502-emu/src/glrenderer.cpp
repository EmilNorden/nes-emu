#include "window.h"
#include "glrenderer.h"
#include <array>

GLRenderer *activeScriptRenderer;

GLRenderer::GLRenderer()
	: context_(nullptr)
{
}

GLRenderer::~GLRenderer()
{
	if(context_ != nullptr)
		SDL_GL_DeleteContext(context_);
}

void GLRenderer::init(const Window &window)
{
	context_ = SDL_GL_CreateContext(window.getSDLWindow());

	// TODO: Kasta lite exceptions till h�ger och v�nster!
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	glViewport(0, 0, window.width(), window.height());
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, window.width(), window.height(), 0.0f, -1.0, 1.0f);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
}

void GLRenderer::clear() const
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void GLRenderer::present(const Window &window) const
{
	SDL_GL_SwapWindow(window.getSDLWindow());
}

void GLRenderer::renderTexture(GLuint texture, int x, int y, int w, int h) const
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
		//Bottom-left vertex (corner)
		glTexCoord2i(0, 0);
		glVertex3f(0, 0, 0.0f);
 
		//Bottom-right vertex (corner)
		glTexCoord2i(1, 0);
		glVertex3f(static_cast<GLfloat>(w), 0, 0.f);
 
		//Top-right vertex (corner)
		glTexCoord2i(1, 1);
		glVertex3f(static_cast<GLfloat>(w), static_cast<GLfloat>(h), 0.f);
 
		//Top-left vertex (corner)
		glTexCoord2i(0, 1);
		glVertex3f(0, static_cast<GLfloat>(h), 0.f);
	glEnd();
}