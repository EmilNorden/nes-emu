#include "cartridge.h"
#include <fstream>

#define TRAINER_SIZE	512
#define PRG_ROM_SIZE	16384
#define CHR_ROM_SIZE	8192
#define INST_ROM_SIZE	8192

Cartridge::Cartridge(std::basic_istream<char, std::char_traits<char>> &in)
{
	header_.Parse(in);

	if(header_.trainer_present())
	{
		trainer_ = std::unique_ptr<char[]>(new char[TRAINER_SIZE]);
		in.read(trainer_.get(), TRAINER_SIZE);
	}

	prg_rom_ = std::unique_ptr<char[]>(new char[PRG_ROM_SIZE * header_.prg_banks()]);
	chr_rom_ = std::unique_ptr<char[]>(new char[CHR_ROM_SIZE * header_.chr_banks()]);

	in.read(prg_rom_.get(), PRG_ROM_SIZE * header_.prg_banks());
	in.read(chr_rom_.get(), CHR_ROM_SIZE * header_.chr_banks());

	uint8_t a = prg_rom_[16381];
	uint8_t b = prg_rom_[16380];


	uint16_t addr = (a << 8) + b;
}