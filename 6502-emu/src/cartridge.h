#ifndef CARTRIDGE_H_
#define CARTRIDGE_H_

#include <iosfwd>
#include <memory>

#include "ines.h"
#include "mappers/mapper.h"

class Mapper;

class Cartridge
{
private:
	iNesHeader header_;
	std::unique_ptr<Mapper> mapper_;
	std::unique_ptr<char[]> trainer_;
	std::unique_ptr<char[]> prg_rom_;
	std::unique_ptr<char[]> chr_rom_;
	std::unique_ptr<char[]> inst_rom_;
	std::unique_ptr<char[]> prom_;
public:
	Cartridge(std::basic_istream<char, std::char_traits<char>> &in);

	iNesHeader header() { return header_; }
	const char *chr_rom() { return chr_rom_.get(); }
	const char *prg_rom() { return prg_rom_.get(); }
};

#endif