#ifndef NES_H_
#define NES_H_

#include "cpu-6502.h"
#include "ppu-2C02.h"

class Cartridge;

class Nes
{
private:
	Cpu6502 cpu_;
	
public:
	Ppu2C02 ppu_;
	Nes();

	void load(Cartridge &c);
	void runOnce();

	const uint32_t *back_buffer() const;
};

#endif