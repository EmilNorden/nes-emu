#ifndef CPU6502_H_
#define CPU6502_H_

#include <array>
#include <cstdint>
#include "cpu_memory_controller.h"

typedef uint8_t opcode;
typedef uint16_t address;

class Cartridge;

class Cpu6502
{
private:
	static const int InterruptVector = 0xFFFE;
	static const int ResetVector = 0xFFFC;
	
	uint8_t accumulator_, x_, y_;
	uint8_t status_;
	uint16_t pc_;
	uint16_t stack_ptr_;

	opcode readOpcode();
	void push8BitStack(uint8_t value);
	void push16BitStack(uint16_t value);

	uint8_t pop8BitStack();
	uint16_t pop16BitStack();

	bool carryFlagSet() const { return status_ & 0x01; }
	bool negativeFlagSet() const { return status_ & 0x40; }
	bool zeroFlagSet() const { return status_ & 0x02; }

	void clearStatusFlags();
	void setBreakFlag();
	void setCarryFlag();
	void setNegativeFlag();
	void setZeroFlag();
	void setInteruptDisableFlag();
	void clearCarryFlag();
	void clearDecimalModeFlag();
	void setZeroNegativeFlags(uint8_t value);

public:
	CpuMemoryController memory_;
	Cpu6502();

	int processInstruction();
	void load(Cartridge &c);
	void reset();

	friend class OpCodes;
};


#endif