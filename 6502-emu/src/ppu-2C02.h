#ifndef PPU2C02_H_
#define PPU2C02_H_

#include <array>
#include <cstdint>
typedef uint16_t address;

class Cartridge;

enum PpuRegister
{
	PPUCTRL = 0,
	PPUMASK,
	PPUSTATUS,
	OAMADDR,
	OAMDATA,
	PPUSCROLL,
	PPUADDR,
	PPUDATA,
	PpuRegisterCount
};
class PpuMemoryController
{
private:
	address current_vram_addr_;
	bool first_write_;
	uint16_t ppuaddr_increment_;
	uint16_t base_nametable_addr_;
	bool vblank_nmi_;
	
	uint8_t ppu_registers_[PpuRegister::PpuRegisterCount];

	/*
	* $0000 - $0FFF Sprite Pattern Table
	* $1000 - $1FFF Background Pattern Table
	* $2000 - $2FFF Name/Attribute Tables
	* $3F00 - $3FFF Palettes
	*/
	std::array<uint8_t, 0x4000> ram_;
public:
	uint16_t t_, v_;
	uint8_t x_;

	PpuMemoryController();

	void copyHorizontalScrollingBits();
	void copyVerticalScrollingBits();
	void loadRom(const char *data);
	void incrementX();
	void incrementY();
	void clearVBlank();
	void setVBlank();


	uint8_t read8Bit(address addr);
	uint16_t read16Bit(address addr);
	uint8_t write(address addr, uint8_t value);

	void ppuctrlWrite(address addr, uint8_t value); // $2000
	void ppumaskWrite(address addr, uint8_t value); // $2001
	void oamaddrWrite(address addr, uint8_t value); // $2003
	void oamdataWrite(address addr, uint8_t value); // $2004
	void ppuscrollWrite(address addr, uint8_t value); // $2005
	void ppuaddrWrite(address addr, uint8_t value); // $2006
	void ppudataWrite(address addr, uint8_t value); // $2007
};

class Ppu2C02
{
private:
	enum class FetchState
	{
		NameTable,
		AttributeTable,
		TileLow,
		TileHigh,
		FetchEnd
	};

	const static int Scanlines = 240;
	const static int LineWidth = 256;
	int scanline_;
	int pixel_;
	int scanline_cycle_;
	int cycle_remainder_;
	FetchState fetch_state_;
	

	uint8_t nt_latch_;
	uint8_t at_latch_;
	uint8_t bg_lo_latch_;
	uint8_t bg_hi_latch_;

	uint16_t background_shift16[2];
	uint8_t background_shift8[2];


	bool render_;

	int fetch_background(int cycles);
	bool is_dummy_scanline() const { return scanline_ == -1; }
	bool is_visible_scanline() const { return scanline_ > -1 && scanline_ < 240; }
	bool is_post_render_scanline() const { return scanline_ == 241; }
	bool is_vblank_scanline() const { return scanline_ > 240 && scanline_ < 261; }
public:
	uint32_t videobuffer_[LineWidth * Scanlines];
	PpuMemoryController memory_;
	int frames;
	Ppu2C02();

	void load(Cartridge &c);
	void reset();

	void process(int cycles);	
	void process2(int cycles);
};

#endif