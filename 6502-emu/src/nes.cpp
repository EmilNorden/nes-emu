#include "nes.h"

#define PPUCTRL_ADDR		0x2000
#define PPUMASK_ADDR		0x2001
#define OAMADDR_ADDR		0x2003
#define OAMDATA_ADDR		0x2004
#define PPUSCROLL_ADDR		0x2005
#define PPUADDR_ADDR		0x2006
#define PPUDATA_ADDR		0x2007


Nes::Nes()
{
	// Set up callbacks for PPU I/O registers
	cpu_.memory_.setWriteCallback(PPUCTRL_ADDR, std::bind(&PpuMemoryController::ppuctrlWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(PPUMASK_ADDR, std::bind(&PpuMemoryController::ppumaskWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(OAMADDR_ADDR, std::bind(&PpuMemoryController::oamaddrWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(OAMDATA_ADDR, std::bind(&PpuMemoryController::oamdataWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(PPUSCROLL_ADDR, std::bind(&PpuMemoryController::ppuscrollWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(PPUADDR_ADDR, std::bind(&PpuMemoryController::ppuaddrWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
	cpu_.memory_.setWriteCallback(PPUDATA_ADDR, std::bind(&PpuMemoryController::ppudataWrite, &ppu_.memory_, std::placeholders::_1, std::placeholders::_2));
}

void Nes::load(Cartridge &c)
{
	cpu_.load(c);
	ppu_.load(c);

	cpu_.reset();
}

void Nes::runOnce()
{
	//int cpu_cycles = cpu_.processInstruction();
	int cpu_cycles = 6;
	int ppu_cycles = cpu_cycles * 3;
	ppu_.process(ppu_cycles);
}


const uint32_t *Nes::back_buffer() const
{
	return ppu_.videobuffer_;
}