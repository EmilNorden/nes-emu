#include <memory>

#include "mapper_factory.h"
#include "mapper_mmc3_mmc6.h"

std::unique_ptr<Mapper> MapperFactory::CreateMapper(const MapperType &type)
{
	switch(type)
	{
	case MapperType::MMC3_MM6:
		return std::unique_ptr<Mapper>(new Mapper_MMC3_MMC6);
		break;
	default:
		return std::unique_ptr<Mapper>();
	}
}