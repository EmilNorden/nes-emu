#ifndef MAPPER_FACTORY_H_
#define MAPPER_FACTORY_H_

#include <memory>
#include "mapper.h"

class MapperFactory
{
public:
	static std::unique_ptr<Mapper> CreateMapper(const MapperType &type);
};

#endif